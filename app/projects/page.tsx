import ProjectCard from '../project-card'
import WidgetReferences from '@/components/widget-references'

import Icon01 from '@/public/images/jukeblox.png'
import Icon02 from '@/public/images/instagram_logo.png'

export const metadata = {
  title: 'Jordan Lewis - Projects',
  description: '',
}

export default function Projects() {

  const items01 = [
    {
      id: 0,
      icon: Icon01,
      slug: 'https://www.linkedin.com/feed/update/urn:li:activity:7091203058222694400/',
      title: 'Jukeblox',
      excerpt: 'Originally made in 48 hours for the GMTK Game Jam 2023, significant updates since.',
      // openSource: true,
    },
  ]

  const items02 = [
    {
      id: 0,
      icon: Icon02,
      slug: 'https://www.instagram.com/windward_mtb/reels',
      title: 'Mountain Bike Instagram Page',
      excerpt: 'A creative outlet for me to edit and share helmet-cam recordings of my rides.',
      openSource: false,
    },
  ]

  return (
    <div className="grow md:flex space-y-8 md:space-y-0 md:space-x-8 pt-12 md:pt-16 pb-16 md:pb-20">

      { /* Middle area */}
      <div className="grow">
        <div className="max-w-[700px]">

          <section>
            {/* Page title */}
            <h1 className="h1 font-aspekta mb-12">Personal projects</h1>
            {/* Page content */}
            <div className="space-y-10">
              {/* Side Hustles cards */}
              <section>
                <h2 className="font-aspekta text-xl font-[650] mb-6">Games</h2>
                {/* Cards */}
                <div className="grid sm:grid-cols-2 md:grid-cols-1 lg:grid-cols-2 gap-5">

                  {items01.map(item => (
                    <ProjectCard key={item.id} item={item} />
                  ))}

                </div>
              </section>
              {/* Client Projects cards */}
              <section>
                <h2 className="font-aspekta text-xl font-[650] mb-6">Other</h2>
                <div className="grid sm:grid-cols-2 md:grid-cols-1 lg:grid-cols-2 gap-5">

                  {items02.map(item => (
                    <ProjectCard key={item.id} item={item} />
                  ))}

                </div>
              </section>
            </div>
          </section>

        </div>
      </div>

      { /* Right sidebar */}
      <aside className="md:w-[240px] lg:w-[300px] shrink-0">
        <div className="space-y-6">

          {/* <WidgetNewsletter /> */}
          {/* <WidgetSponsor /> */}
          <WidgetReferences />

        </div>
      </aside>

    </div>
  )
}
