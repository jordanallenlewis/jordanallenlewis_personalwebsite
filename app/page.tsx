import { allPosts } from 'contentlayer/generated'
import Hero from '@/components/hero'
import PostItem from './post-item'
import Talks from '@/components/talks'
import FeaturedProjects from '@/components/featured-projects'
import WidgetNewsletter from '@/components/widget-newsletter'
import WidgetSponsor from '@/components/widget-sponsor'
import WidgetBook from '@/components/widget-book'
import WidgetReferences from '@/components/widget-references'

import Image from 'next/image'
// import WidgetNewsletter from '@/components/widget-newsletter'
// import WidgetSponsor from '@/components/widget-sponsor'
import HomeImg from '@/public/images/home.png'
import Experience from '@/components/experience'
import Education from '@/components/education'
import HeroImage from '@/public/images/me.jpg'

export const metadata = {
  title: 'Jordan Lewis - Home',
  description: '',
}

export default async function Home() {

  // Sort posts by date
  allPosts.sort((a, b) => {
    return (new Date(a.publishedAt) > new Date(b.publishedAt)) ? -1 : 1
  }) 

  return (
    <>
    {/* <Hero /> */}
    <div className="grow md:flex space-y-8 md:space-y-0 md:space-x-8 pt-12 md:pt-16 pb-16 md:pb-20">
      { /* Middle area */}
      <div className="grow">
        <div className="max-w-[700px]">

          <section>
          {/* <Image className="rounded-full mb-5" src={HeroImage} width={56} height={56} priority alt="Me" /> */}
            { /* Page title */}
            <h1 className="h1 font-aspekta mb-5">Hey, I'm <span className="inline-flex relative text-sky-500 before:absolute before:inset-0 before:bg-sky-200 dark:before:bg-sky-500 before:opacity-30 before:-z-10 before:-rotate-2 before:translate-y-1/4">Jordan</span> 👋🏻</h1>
            <Image className="w-full" src={HomeImg} width={400} height={390} alt="About" />
            { /* Page content */}
            <div className="text-slate-500 dark:text-slate-400 space-y-8">
              <div className="space-y-4">
                {/* <h2 className="h3 font-aspekta text-slate-800 dark:text-slate-100">Short Bio</h2> */}
                <p>
I’m a Data Engineer with experience working on data infrastructure and improving data accessibility. I’ve been involved in deploying and managing data platforms, using tools like Kubernetes, APIs, and data virtualization solutions to help support data-driven projects.
<br/><br/>I enjoy collaborating with teams like DevOps, IT, and governance to improve deployment processes, performance monitoring, and secure data access. I’m always eager to solve data challenges and find ways to make data more accessible and useful within organizations.
<br/><br/>Outside of work, I enjoy hitting the trails on my mountain bike and exploring new adventures!
                </p>
                <p>
                  {/* While there isn't a Wikipedia page about me (sorry folks!), a media bio is available below. */}
                </p>
              </div>
              {/* <div className="space-y-4">
                <h2 className="h3 font-aspekta text-slate-800 dark:text-slate-100">About Me</h2>
                <p>At <a className="font-medium text-sky-500 hover:underline" href="https://www.vericast.com/" target="_blank" rel="noopener noreferrer">Vericast</a>, in my role as a Data Infrastructure Engineer, I've been deeply involved in several key projects that underscore the company's commitment to innovative data management and utilization. One of my primary achievements has been leading the development of the foundational elements within our <a className="font-medium text-sky-500 hover:underline" href="https://www.collibra.com/" target="_blank" rel="noopener noreferrer">Collibra</a> data catalog, an initiative that required both creative thinking and technical acumen to automate and streamline data processes. Furthermore, I pioneered the creation of our first APIs using <a className="font-medium text-sky-500 hover:underline" href="https://www.mulesoft.com/" target="_blank" rel="noopener noreferrer">Mulesoft</a>, which has significantly enhanced our ability to integrate and manage data across various platforms efficiently. Another notable contribution includes rejuvenating an internal data orchestration web application, a project that not only revitalized an essential tool but also demonstrated the potential for improving data visualization and accessibility across the organization.</p>
                <p>Before my tenure at <a className="font-medium text-sky-500 hover:underline" href="https://www.vericast.com/" target="_blank" rel="noopener noreferrer">Vericast</a>, I honed my skills and leadership at <a className="font-medium text-sky-500 hover:underline" href="https://www.usaa.com/" target="_blank" rel="noopener noreferrer">USAA</a>. There, I was instrumental in leading a team towards optimizing data processes, an experience that was both challenging and rewarding. This role provided me with a robust framework for understanding the complexities of data engineering in a large organization and the importance of developing scalable and efficient solutions. The skills and insights gained at <a className="font-medium text-sky-500 hover:underline" href="https://www.usaa.com/" target="_blank" rel="noopener noreferrer">USAA</a> have been invaluable in my current role, allowing me to tackle complex projects with confidence and innovation.</p>
                <p>My academic pursuits, including a Master’s in Computer Science from the <a className="font-medium text-sky-500 hover:underline" href="https://www.gatech.edu/" target="_blank" rel="noopener noreferrer">Georgia Institute of Technology</a> and an undergraduate degree from <a className="font-medium text-sky-500 hover:underline" href="https://www.asu.edu/" target="_blank" rel="noopener noreferrer">Arizona State University</a>, laid the groundwork for my professional endeavors. The rigorous curriculum and hands-on projects prepared me for the challenges of the tech industry, instilling a solid foundation in both theory and practical application. This educational background, combined with my professional experiences, has equipped me with a unique perspective on data engineering, driving my passion for pushing the boundaries of what's possible in data infrastructure and management.</p>
              </div> */}
              <Education />
              <Experience />

              {/* <div className="space-y-4">
                <h2 className="h3 font-aspekta text-slate-800 dark:text-slate-100">Let's Connect</h2>
                <p>
                  You can find me on <a className="font-medium text-sky-500 hover:underline" href="https://www.linkedin.com/in/jordanallenlewis/" target="_blank" rel="noopener noreferrer">LinkedIn</a> or reach out via email at JordanAllenLewis@gmail.com. Whether it's to discuss potential projects or simply to share insights and experiences, I'm always open to hearing from you. Feel free to drop me a message anytime!
                </p>
              </div> */}
            </div>
          </section>

        </div>
      </div>

      { /* Right sidebar */}
      <aside className="md:w-[240px] lg:w-[300px] shrink-0">
        <div className="space-y-6">
          <WidgetReferences />
        </div>
      </aside>
      </div>
    </>
  )
}
