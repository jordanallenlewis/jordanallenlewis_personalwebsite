import Image from 'next/image'

import Reference01 from '@/public/images/resume_icon.png'
import Reference02 from '@/public/images/linkedin_icon.png'

export default function WidgetReferences() {
  return (
    <div className="fixed top-1/7 right-20 max-w-[300px] w-full rounded-lg border border-slate-200 dark:border-slate-800 dark:bg-gradient-to-t dark:from-slate-800 dark:to-slate-800/30 p-5 shadow-lg">
      <div className="font-aspekta font-[650] mb-3">Links</div>
      <ul className="space-y-3">
        <li>
          <a className="flex justify-between items-center space-x-2 group" href="/files/JordanAllenLewis_Resume.pdf" target="_blank" rel="noopener noreferrer">
            <div className="grow flex items-center space-x-3 truncate">
              <Image src={Reference01} width={32} height={32} alt="Mr. Mark Smularkov" />
              <div className="truncate">
                <div className="font-aspekta font-[650] text-sm truncate mb-1">Resume</div>
                <div className="text-xs text-slate-500 dark:text-slate-400">PDF</div>
              </div>
            </div>
            <div className="shrink-0 text-sky-500">
              <svg xmlns="http://www.w3.org/2000/svg" className="w-6 h-6 fill-current group-hover:rotate-45 transition-transform ease-out" width="14" height="12">
                <path d="M18.97 3.659a2.25 2.25 0 0 0-3.182 0l-10.94 10.94a3.75 3.75 0 1 0 5.304 5.303l7.693-7.693a.75.75 0 0 1 1.06 1.06l-7.693 7.693a5.25 5.25 0 1 1-7.424-7.424l10.939-10.94a3.75 3.75 0 1 1 5.303 5.304L9.097 18.835l-.008.008-.007.007-.002.002-.003.002A2.25 2.25 0 0 1 5.91 15.66l7.81-7.81a.75.75 0 0 1 1.061 1.06l-7.81 7.81a.75.75 0 0 0 1.054 1.068L18.97 6.84a2.25 2.25 0 0 0 0-3.182Z"></path>
              </svg>
            </div>
          </a>
        </li>
        <li>
          <a className="flex justify-between items-center space-x-2 group" href="https://www.linkedin.com/in/jordanallenlewis/" target="_blank" rel="noopener noreferrer">
            <div className="grow flex items-center space-x-3 truncate">
              <Image className="rounded-full" src={Reference02} width={32} height={32} alt="Jame Kulls" />
              <div className="truncate">
                <div className="font-aspekta font-[650] text-sm truncate mb-1">LinkedIn Profile</div>
                <div className="text-xs text-slate-500 dark:text-slate-400">Link</div>
              </div>
            </div>
            <div className="shrink-0 text-sky-500">
              <svg className="fill-current -rotate-45 group-hover:rotate-0 transition-transform ease-out" xmlns="http://www.w3.org/2000/svg" width="14" height="12">
                <path d="M9.586 5 6.293 1.707 7.707.293 13.414 6l-5.707 5.707-1.414-1.414L9.586 7H0V5h9.586Z"></path>
              </svg>
            </div>
          </a>
        </li>
      </ul>
    </div>
  )
}