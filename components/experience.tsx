'use client'
import Image from 'next/image'
import VericastLogo from '@/public/images/RR_Donnelley_logo.png'
import USAALogo from '@/public/images/USAA_Logo.png'
import { useEffect, useState } from 'react';

export default function Experience() {
  const [durationVericastSecond, setDurationVericastSecond] = useState('');
  const [durationVericast, setDurationVericast] = useState('');
  const [durationUSAAFirst, setDurationUSAAFirst] = useState('');
  const [durationUSAASec, setDurationUSAASec] = useState('');
  const [durationUSAAThird, setDurationUSAAThird] = useState('');

  useEffect(() => {
    const calculateDuration = (start: string, end: string) => {
      const startDate = new Date(start);
      const endDate = end.toLowerCase() === 'present' ? new Date() : new Date(end);
      let totalMonths = (endDate.getFullYear() - startDate.getFullYear()) * 12 + endDate.getMonth() - startDate.getMonth();
      const dayDifference = endDate.getDate() - startDate.getDate();

      // Adjust for days that might push the month count up
      if (dayDifference > 0) {
        totalMonths += 1;
      }

      const years = Math.floor(totalMonths / 12);
      const months = totalMonths % 12; // Adjusted to handle overflow properly

      let durationText = " (";
      if (years > 0) {
        durationText += years + " year" + (years > 1 ? "s" : "");
      }
      if (months > 0) {
        if (years > 0) durationText += ", ";
        durationText += months + " month" + (months > 1 ? "s" : "");
      }
      durationText += ")";

      return durationText;
    };
    setDurationVericastSecond(calculateDuration('May 1, 2024', 'Present'));
    setDurationVericast(calculateDuration('August 1, 2022', 'May 1, 2024'));
    setDurationUSAAFirst(calculateDuration('May 1, 2022', 'July 31, 2022'));
    setDurationUSAASec(calculateDuration('August 1, 2021', 'May 31, 2022'));
    setDurationUSAAThird(calculateDuration('September 1, 2019', 'August 31, 2021'));
  }, []);

  return (
    <div className="space-y-8">
      <h2 className="h3 font-aspekta text-slate-800 dark:text-slate-100">Work Experience</h2>
      <ul className="space-y-8">
        {/* Item */}
        <li className="relative group">
  <div className="flex items-start before:absolute before:left-0 before:h-full before:w-px before:bg-slate-200 before:dark:bg-slate-800 before:self-start before:ml-[28px] before:-translate-x-1/2 before:translate-y-8 before:group-last-of-type:hidden">
    <div className="absolute left-0 h-14 w-14 flex items-center justify-center border border-slate-200 dark:border-slate-800 dark:bg-gradient-to-t dark:from-slate-800 dark:to-slate-800/30 bg-white dark:bg-slate-900 rounded-full">
      <Image src={VericastLogo} width={35} alt="RRD" />
    </div>
    <div className="pl-20 space-y-1">
      <div className="text-xs text-slate-500 uppercase">
        May 2024 <span className="text-slate-400 dark:text-slate-600">·</span> Present
        <span className="text-xs text-slate-500 lowercase">{durationVericastSecond}</span>
      </div>
      <div className="font-aspekta font-[650] text-slate-800 dark:text-slate-100">Data Infrastructure Engineer II</div>
      <div className="text-sm font-medium text-slate-800 dark:text-slate-100">RR Donnelley <span className="text-xs text-slate-500">(Remote)</span></div>
      <div className="text-sm text-slate-500 dark:text-slate-400">
        <ul>
          <li>Serving as a full-stack, versatile member of a highly agile team, contributing to many facets of our Data Mesh initiative.</li>
          <li>Deployed and currently managing Dremio via Helm charts on an on-premises Kubernetes cluster as a proof of concept during product vetting, setting up JMX metrics, Prometheus, and Grafana dashboards for real-time performance monitoring.</li>
          <li>Implemented single sign-on (SSO) and LDAP role-based authorization for Dremio to enforce secure and automated user access, ensuring permissions align with team-based governance policies.</li>
          <li>Created MuleSoft API proxies on top of internal minion services with authentication through OneLogin and an internal auth minion service, facilitating secure data sharing across multiple acquired organizations.</li>
          <li>Enhanced the internal Data Mesh website by integrating MuleSoft APIs with Dremio, enabling users to submit queries and retrieve results directly through the website, improving data accessibility and demonstrating the power of the Data Mesh.</li>
          <li>Collaborating with IT teams to open networking pathways between Dremio and multiple on-premises Hadoop clusters, ensuring seamless data integration and accessibility for end users.</li>
          <li>Partnering closely with DevOps, IT, and Data Platform teams to optimize deployment, integration, and maintenance of Data Mesh components, coordinating weekly troubleshooting calls with Dremio support to resolve challenges and improve system stability.</li>
          <li>Implementing data governance practices by working with the governance team to ensure all imported data is properly classified, maintaining compliance and enhancing data discoverability across the organization.</li>
          <li>Continuing my work and responsibilities listed below as a Data Infrastructure Engineer I.</li>
          <li>Primary technologies: Collibra, Docker, Dremio, Git CI/CD, Grafana, Hadoop, Helm, JMX, Jenkins, Kubernetes, LDAP, MuleSoft, Next.js, OneLogin, Prometheus, Python, REST APIs, Snowflake, Unix.</li>
        </ul>
      </div>
    </div>
  </div>
</li>
        {/* Item */}
        <li className="relative group">
  <div className="flex items-start before:absolute before:left-0 before:h-full before:w-px before:bg-slate-200 before:dark:bg-slate-800 before:self-start before:ml-[28px] before:-translate-x-1/2 before:translate-y-8 before:group-last-of-type:hidden">
    <div className="absolute left-0 h-14 w-14 flex items-center justify-center border border-slate-200 dark:border-slate-800 dark:bg-gradient-to-t dark:from-slate-800 dark:to-slate-800/30 bg-white dark:bg-slate-900 rounded-full">
      <Image src={VericastLogo} width={35} alt="RR Donnelley" />
    </div>
    <div className="pl-20 space-y-1">
      <div className="text-xs text-slate-500 uppercase">
        Aug 2022 <span className="text-slate-400 dark:text-slate-600">·</span> May 2024
        <span className="text-xs text-slate-500 lowercase">{durationVericast}</span>
      </div>
      <div className="font-aspekta font-[650] text-slate-800 dark:text-slate-100">Data Infrastructure Engineer I</div>
      <div className="text-sm font-medium text-slate-800 dark:text-slate-100">RR Donnelley <span className="text-xs text-slate-500">(Remote)</span></div>
      <div className="text-sm text-slate-500 dark:text-slate-400">
        <ul>
          <li>Creating a centralized data catalog, providing a unified view of company-wide data assets.</li>
          <li>Developing APIs to ensure seamless data access, contributing to a more responsive and user-friendly data ecosystem.</li>
          <li>Integrating existing internal websites with the Data Mesh, enhancing user experiences across the organization.</li>
          <li>Configuring ETL pipelines for consumers within the company, as well as Data Products on the public Snowflake Marketplace.</li>
          <li>Developed and launched an internal Next.js 14 web application, serving as a landing page/information hub for the Data Mesh project. Features real-time API calls to display live stats on number of data products, data sets, columns, etc.</li>
          <li>Authoring blog-style Data Mesh Guild posts which effectively communicate new deployments and key updates to stakeholders across the enterprise, enhancing cross-functional collaboration and driving informed decision-making.</li>
          <li>Assessing, deploying internally to Kubernetes/OpenStack, and creating compelling demos for technologies which have potential to enhance the Data Mesh.</li>
          <li>Participating in evaluations of various technologies for data virtualization, visualization, event streaming, generative AI, etc.</li>
          <li>Primary technologies: Collibra, Docker, Git CI/CD, Hadoop, Jenkins, MuleSoft, Next.js, Python, Snowflake, Unix.</li>
        </ul>
      </div>
    </div>
  </div>
</li>
        {/* Item */}
        <li className="relative group">
          <div className="flex items-start before:absolute before:left-0 before:h-full before:w-px before:bg-slate-200 before:dark:bg-slate-800 before:self-start before:ml-[28px] before:-translate-x-1/2 before:translate-y-8 before:group-last-of-type:hidden">
            <div className="absolute left-0 h-14 w-14 flex items-center justify-center border border-slate-200 dark:border-slate-800 dark:bg-gradient-to-t dark:from-slate-800 dark:to-slate-800/30 bg-white dark:bg-slate-900 rounded-full">
              <Image src={USAALogo} width={40} alt="USAA" />
            </div>
            <div className="pl-20 space-y-1">
              <div className="text-xs text-slate-500 uppercase">May 2022 <span className="text-slate-400 dark:text-slate-600">·</span> July 2022 
                <span className="text-xs text-slate-500 lowercase">{durationUSAAFirst}</span>
              </div>
              <div className="font-aspekta font-[650] text-slate-800 dark:text-slate-100">Tech Lead - Software Engineer II</div>
              <div className="text-sm font-medium text-slate-800 dark:text-slate-100">USAA <span className="text-xs text-slate-500">(Plano, TX)</span></div>
              <div className="text-sm text-slate-500 dark:text-slate-400">
                <ul>
                  <li>Leading a team of 5 globally diverse engineers in development features and code reviews. </li>
                  <li>Creating technical designs and product roadmaps to drive reliable, reusable, and scalable solutions.</li>
                  <li>Continuing my responsibilities listed below as a Software Engineer II.</li>
                </ul>
              </div>
            </div>
          </div>
        </li>
        {/* Item */}
        <li className="relative group">
          <div className="flex items-start before:absolute before:left-0 before:h-full before:w-px before:bg-slate-200 before:dark:bg-slate-800 before:self-start before:ml-[28px] before:-translate-x-1/2 before:translate-y-8 before:group-last-of-type:hidden">
            <div className="absolute left-0 h-14 w-14 flex items-center justify-center border border-slate-200 dark:border-slate-800 dark:bg-gradient-to-t dark:from-slate-800 dark:to-slate-800/30 bg-white dark:bg-slate-900 rounded-full">
              <Image src={USAALogo} width={40} alt="USAA" />
            </div>
            <div className="pl-20 space-y-1">
              <div className="text-xs text-slate-500 uppercase">Aug 2021 <span className="text-slate-400 dark:text-slate-600">·</span> May 2022 
              <span className="text-xs text-slate-500 lowercase">{durationUSAASec}</span></div>
              <div className="font-aspekta font-[650] text-slate-800 dark:text-slate-100">Software Engineer II</div>
              <div className="text-sm font-medium text-slate-800 dark:text-slate-100">USAA <span className="text-xs text-slate-500">(Plano, TX)</span></div>
              <div className="text-sm text-slate-500 dark:text-slate-400">
                <ul>
                  <li>Developing small batch data load and model processes using Domino, R, Python, Git CI/CD, and Airflow.</li>
                  <li>First team in organization to transition business developed models to IT supported infrastructure/technology, benefiting in 60% reduction in code and 95% reduction in hours worked monthly (~$84,000 saved per year per model).</li>
                  <li>Programming ETL jobs using Python, shell scripts, and IBM DataStage.</li>
                  <li>Developing and maintaining data integrity through run time controls over various data hops.</li>
                  <li>Instructing quarterly course to bring employees up-to-speed on IBM DataStage as well as ETL methodologies.</li>
                  <li>Primary technologies: Airflow, Control-M, DataStage, Domino, Git CI/CD, Python, R, Tableau, Unix.</li>
                </ul>
              </div>
            </div>
          </div>
        </li>
        {/* Item */}
        <li className="relative group">
          <div className="flex items-start before:absolute before:left-0 before:h-full before:w-px before:bg-slate-200 before:dark:bg-slate-800 before:self-start before:ml-[28px] before:-translate-x-1/2 before:translate-y-8 before:group-last-of-type:hidden">
            <div className="absolute left-0 h-14 w-14 flex items-center justify-center border border-slate-200 dark:border-slate-800 dark:bg-gradient-to-t dark:from-slate-800 dark:to-slate-800/30 bg-white dark:bg-slate-900 rounded-full">
              <Image src={USAALogo} width={40} alt="USAA" />
            </div>
            <div className="pl-20 space-y-1">
              <div className="text-xs text-slate-500 uppercase">Sep 2019 <span className="text-slate-400 dark:text-slate-600">·</span> Aug 2021 
              <span className="text-xs text-slate-500 lowercase">{durationUSAAThird}</span></div>
              <div className="font-aspekta font-[650] text-slate-800 dark:text-slate-100">Software Engineer III</div>
              <div className="text-sm font-medium text-slate-800 dark:text-slate-100">USAA <span className="text-xs text-slate-500">(Plano, TX)</span></div>
              <div className="text-sm text-slate-500 dark:text-slate-400">
                <ul>
                  <li>Working systems that supply data to USAA data analysts for reporting.</li>
                  <li>Creating data pipelines from application databases to enterprise data warehouse.</li>
                  <li>Developing and maintaining data integrity through run time controls over various data hops.</li>
                  <li>Programming ETL jobs using Python, shell scripts, and IBM DataStage.</li>
                  <li>Instructing quarterly course to bring employees up-to-speed on IBM DataStage as well as ETL methodologies.</li>
                  <li>Primary technologies: Oracle, Python, DataStage, Control-M, Tableau.</li>
                </ul>
              </div>
            </div>
          </div>
        </li>
        {/* Item */}
        {/* <li className="relative group">
          <div className="flex items-start before:absolute before:left-0 before:h-full before:w-px before:bg-slate-200 before:dark:bg-slate-800 before:self-start before:ml-[28px] before:-translate-x-1/2 before:translate-y-8 before:group-last-of-type:hidden">
            <div className="absolute left-0 h-14 w-14 flex items-center justify-center border border-slate-200 dark:border-slate-800 dark:bg-gradient-to-t dark:from-slate-800 dark:to-slate-800/30 bg-white dark:bg-slate-900 rounded-full">
              <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17">
                <path fill="#6366F1" d="M2.486 5.549C3.974 7.044 5.953 7.89 8.009 8.045c-.138-2.065-.997-4.053-2.486-5.548C4.035 1.002 2.117.154 0 0c.138 2.065.997 4.053 2.486 5.549Zm12.028 0c-1.488 1.495-3.467 2.342-5.523 2.496.138-2.065.997-4.053 2.486-5.548C12.888 1.002 14.883.154 17 0c-.153 2.065-.997 4.053-2.486 5.549Zm0 5.902c-1.488-1.495-3.467-2.342-5.523-2.496.138 2.065.997 4.053 2.486 5.548C12.965 15.998 14.944 16.846 17 17c-.153-2.127-.997-4.13-2.486-5.549Zm-12.028 0c1.488-1.495 3.467-2.342 5.6-2.496-.138 2.065-.998 4.053-2.486 5.548C4.035 15.998 2.117 16.861 0 17c.138-2.127.997-4.13 2.486-5.549Z" />
              </svg>
            </div>
            <div className="pl-20 space-y-1">
              <div className="text-xs text-slate-500 uppercase">Feb 2016 <span className="text-slate-400 dark:text-slate-600">·</span> Dec 2016</div>
              <div className="font-aspekta font-[650] text-slate-800 dark:text-slate-100">Cloud Software Engineering Intern</div>
              <div className="text-sm font-medium text-slate-800 dark:text-slate-100">Qonto</div>
              <div className="text-sm text-slate-500 dark:text-slate-400">In my role as a Senior Software Engineer for Google, I am responsible for developing and maintaining the Chrome Web Experience.</div>
            </div>
          </div>
        </li> */}
      </ul>
    </div>
  )
}