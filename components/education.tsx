import Image from 'next/image'

import EducationIcon01 from '@/public/images/gt_logo.png'
import EducationIcon02 from '@/public/images/asu_logo.png'

export default function Education() {
  return (
    <div className="space-y-8">
      <h2 className="h3 font-aspekta text-slate-800 dark:text-slate-100">Education</h2>
      <ul className="space-y-8">
        {/* Item */}
        <li className="relative group">
          <div className="flex items-start before:absolute before:left-0 before:h-full before:w-px before:bg-slate-200 before:dark:bg-slate-800 before:self-start before:ml-[28px] before:-translate-x-1/2 before:translate-y-8 before:group-last-of-type:hidden">
            <div className="absolute left-0 h-14 w-14 flex items-center justify-center border border-slate-200 dark:border-slate-800 dark:bg-gradient-to-t dark:from-slate-800 dark:to-slate-800/30 bg-white dark:bg-slate-900 rounded-full">
              <Image src={EducationIcon01} width={28} height={24} alt="Georgia Institute of Technology" />
            </div>
            <div className="pl-20 space-y-1">
              <div className="text-xs text-slate-500 uppercase">2024</div>
              <div className="font-aspekta font-[650] text-slate-800 dark:text-slate-100">M.S. Computer Science</div>
              <div className="text-sm font-medium text-slate-800 dark:text-slate-100">Georgia Institute of Technology</div>
              {/* <div className="text-sm text-slate-500 dark:text-slate-400"> 
                <ul>
                  <li>Database Systems, Concepts, and Design</li>
                  <li>Computer Networks</li>
                  <li>Intro to Information Security</li>
                  <li>Software Development Process</li>
                  <li>Software Architecture and Design</li>
                  <li>Software Analysis</li>
                  <li>Game Artificial Intelligence</li>
                  <li>Video Game Design</li>
                  <li>Intro to Graduate Algorithms</li>
                  <li>Human-Computer Interaction</li>
                </ul>
              </div> */}
            </div>
          </div>
        </li>
        {/* Item */}
        <li className="relative group">
          <div className="flex items-start before:absolute before:left-0 before:h-full before:w-px before:bg-slate-200 before:dark:bg-slate-800 before:self-start before:ml-[28px] before:-translate-x-1/2 before:translate-y-8 before:group-last-of-type:hidden">
            <div className="absolute left-0 h-14 w-14 flex items-center justify-center border border-slate-200 dark:border-slate-800 dark:bg-gradient-to-t dark:from-slate-800 dark:to-slate-800/30 bg-white dark:bg-slate-900 rounded-full">
              <Image src={EducationIcon02} width={28} height={26} alt="Arizona State University" />
            </div>
            <div className="pl-20 space-y-1">
              <div className="text-xs text-slate-500 uppercase">2019</div>
              <div className="font-aspekta font-[650] text-slate-800 dark:text-slate-100">B.S. Computer Science</div>
              <div className="text-sm font-medium text-slate-800 dark:text-slate-100">Arizona State University</div>
              {/* <div className="text-sm text-slate-500 dark:text-slate-400">Throughout my years at SJSU, I immersed myself in a dynamic learning environment, surrounded by dedicated faculty and talented peers.</div> */}
            </div>
          </div>
        </li>
      </ul>
    </div>
  )
}